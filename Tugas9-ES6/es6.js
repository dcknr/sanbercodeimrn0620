// Nomor 1

console.log(" ")
console.log("Nomor 1")
console.log(" ")

const golden = () => {
    console.log("this is golden!!")
  }
   
golden()

// Nomor 2

console.log(" ")
console.log("Nomor 2")
console.log(" ")

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
            return 
      }
    }
}
   
//Driver Code 
newFunction("William", "Imoh").fullName() 


// Nomor 3

console.log(" ")
console.log("Nomor 3")
console.log(" ")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

// ES5
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// ES6
const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)


// Nomor 4

console.log(" ")
console.log("Nomor 4")
console.log(" ")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// ES5
// const combined = west.concat(east)

// ES6
const combined = [...west, ...east]
//Driver Code
console.log(combined)


// Nomor 5

console.log(" ")
console.log("Nomor 5")
console.log(" ")

const planet    = "earth"
const view      = "glass"
// var before      = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
 
const before    =   `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before) 