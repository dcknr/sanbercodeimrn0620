// Tugas 3 Looping

// Nomor 1

console.log(" ")
console.log("Nomor 1")
console.log(" ")
console.log("OUTPUT:")
console.log(" ")

console.log("LOOPING PERTAMA");
var max = 2;
while(max <= 20) { 
    if(max%2 == 0){
        console.log(max + ' - I love coding');
    }
    max++;
}

console.log("LOOPING KEDUA");
var min = 20;
while(min >= 2) { 
    if(min%2 == 0){
        console.log(min + ' - I will become a mobile developer');
    }
    min--;
}


// Nomor 2

console.log(" ")
console.log("Nomor 2")
console.log(" ")
console.log("OUTPUT:")
console.log(" ")

for(var angka = 1; angka <= 20; angka++) {
    if(angka%2 == 1){
        if(angka%3 == 0){
            console.log(angka + ' - I Love Coding');
        }
        else{
            console.log(angka + ' - Santai');
        }
    }
    else{
        console.log(angka + ' - Berkualitas');
    }
} 

// Nomor 3

console.log(" ")
console.log("Nomor 3")
console.log(" ")
console.log("OUTPUT:")
console.log(" ")

// Cara 1
// for(var baris = 1; baris <= 4; baris++) {
//     for(var kolom = 1; kolom <= 8; kolom++) {
//         process.stdout.write("#");
//     }  
//     console.log(""); 
// } 

// Cara 2

var hasil= "";
for(var baris = 1; baris <= 4; baris++) {
    for(var kolom = 1; kolom <= 8; kolom++) {
        hasil += "#";
    }  
  hasil += "\n";
} 
console.log(hasil);

// Nomor 4

console.log(" ")
console.log("Nomor 4")
console.log(" ")
console.log("OUTPUT:")
console.log(" ")

var hasil= "";
for(var baris = 1; baris <= 7; baris++) {
    for(var kolom = 1; kolom <= baris; kolom++) {
        hasil += "#";
    }  
  hasil += "\n";
} 
console.log(hasil);


// Nomor 5

console.log(" ")
console.log("Nomor 5")
console.log(" ")
console.log("OUTPUT:")
console.log(" ")

var hasil= "";
for(var baris = 1; baris <= 8; baris++) {
    for(var kolom = 1; kolom <= 8; kolom++) {
      if(baris%2==1){
        if(kolom%2==1){
           hasil += " "; 
        }
        else{
          hasil += "#"; 
        }
      }
      else{
        if(kolom%2==1){
           hasil += "#"; 
        }
        else{
          hasil += " "; 
        }
      }
    }  
  hasil += "\n";
} 
console.log(hasil);
