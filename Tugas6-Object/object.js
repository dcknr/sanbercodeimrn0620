// Nomor 1

console.log(" ")
console.log("Nomor 1")
console.log(" ")

function arrayToObject(arr) {
    if(arr.length == 0 || arr == undefined){
        console.log(" ");
    }
    else{
        var obj = {}
        
        //cara 2
        // var object = {}; 
        
        function age(age){
            var now = new Date()
            var thisYear = now.getFullYear() // 2020 (tahun sekarang)
            
            if(age == undefined || age > thisYear){
                return "Invalid birth year."
            }
            else{
                return thisYear - age
            }
        }

        //Cara 1
        for(var index = 0; index < arr.length; index++) {
            obj.firstName   = arr[index][0];
            obj.lastName    = arr[index][1];
            obj.gender      = arr[index][2];
            obj.age         = age(arr[index][3]);
            
            console.log(index + 1 + ". " + arr[index][0] + " " + arr[index][1] + ":")
            console.log(obj);
        }  

        //Cara 2
        // for(var index = 0; index < arr.length; index++) {
        //     obj.firstName   = arr[index][0];
        //     obj.lastName    = arr[index][1];
        //     obj.gender      = arr[index][2];
        //     obj.age         = age(arr[index][3]);
        //     object[index + 1 + ". " + arr[index][0] + " " + arr[index][1] + ":"] = obj
            
        // }
        // console.log(object);  
    }    
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

// 1. Bruce Banner: { 
//     firstName: "Bruce",
//     lastName: "Banner",
//     gender: "male",
//     age: 45
// }
// 2. Natasha Romanoff: { 
//     firstName: "Natasha",
//     lastName: "Romanoff",
//     gender: "female".
//     age: "Invalid Birth Year"
// }

// Error case 
arrayToObject([]) // ""


// Nomor 2

console.log(" ")
console.log("Nomor 2")
console.log(" ")

function shoppingTime(memberId, money){
    
    var sale = {
        1: { name: 'Sepatu Stacattu',   price:  1500000 },
        2: { name: 'Baju Zoro',         price:  500000 },
        3: { name: 'Baju H&N',          price:  250000 },
        4: { name: 'Sweater Uniklooh',  price:  175000 },
        5: { name: 'Casing Handphone',  price:  50000 }
    };

    var object          = {}
    var listPurchased   = []
    
    if(memberId == undefined || memberId == ""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if(money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }
    else{
        var changeMoney = money;
        for (var index in sale) {
            if(changeMoney >= sale[index].price){
                listPurchased.push(sale[index].name)
                changeMoney = changeMoney - sale[index].price
            }
        }    
        
        object.memberId         =   memberId
        object.money            =   money,
        object.listPurchased    =   listPurchased,
        object.changeMoney      =   changeMoney 

        return object
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja



// Nomor 3

console.log(" ")
console.log("Nomor 3")
console.log(" ")

function naikAngkot(arrPenumpang) {
    rute    = ['A', 'B', 'C', 'D', 'E', 'F']
    list    = []
    
    //your code here
    
    if(arrPenumpang.length == 0 || arrPenumpang == undefined){
        return []
    }
    else{
        for(var index = 0; index < arrPenumpang.length; index++) {
            object              =   {}
            var bayar           =   (rute.indexOf(arrPenumpang[index][2])-rute.indexOf(arrPenumpang[index][1]))*2000

            object.penumpang    =   arrPenumpang[index][0]
            object.naikDari     =   arrPenumpang[index][1]
            object.tujuan       =   arrPenumpang[index][2]
            object.bayar        =   bayar

            list.push(object)
            
        }  
        return list
    }
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]