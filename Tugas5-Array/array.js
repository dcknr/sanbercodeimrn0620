// Nomor 1

console.log(" ")
console.log("Nomor 1")
console.log(" ")

function range(startNum, finishNum) {
    var hasil = [];
    if(startNum < finishNum){ 
        for(var angka = startNum; angka <= finishNum; angka++) {
            hasil.push(angka)
        }  
        return hasil
    }
    else if(startNum > finishNum){ //desc
        for(var angka = startNum; angka >= finishNum; angka--) {
            hasil.push(angka)
        }  
        return hasil
    }
    else if(startNum==undefined || finishNum==undefined){
        return -1
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Nomor 2

console.log(" ")
console.log("Nomor 2")
console.log(" ")

function rangeWithStep(startNum, finishNum, step){
    var hasil = [];
    if(startNum < finishNum){ 
        for(var angka = startNum; angka <= finishNum; angka = angka + step) {
            hasil.push(angka)
        }  
        return hasil
    }
    else if(startNum > finishNum){ //desc
        for(var angka = startNum; angka >= finishNum; angka = angka - step) {
            hasil.push(angka)
        }  
        return hasil
    }
    else if(startNum==undefined || finishNum==undefined || step==undefined ){
        return -1
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Nomor 3

console.log(" ")
console.log("Nomor 3")
console.log(" ")

function sum(startNum, finishNum, step = 1){
    var jumlah = 0;
    if(startNum==undefined && finishNum==undefined){
        return 0
    }
    else if(startNum!=undefined && finishNum!=undefined){
        jumlah = 0; //inisialisasi variabel jumlah
        
        hasil = rangeWithStep(startNum, finishNum, step) //hasil array deret
        
        //jumlahkan deret
        for(i=0;i<hasil.length;i++){
            jumlah = jumlah + hasil[i];
        }
        
        //return deret
        return jumlah;
    }
    else{
        return startNum
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


// Nomor 4

console.log(" ")
console.log("Nomor 4")
console.log(" ")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input=[]){
    for(var index = 0; index < input.length; index++) {
        console.log("Nomor ID       :   " + input[index][0]);
        console.log("Nama Lengkap   :   " + input[index][1]); 
        console.log("TTL            :   " + input[index][2] + " " + input[index][3]); 
        console.log("Hobi           :   " + input[index][4]); 
        console.log(" "); 
    }  
}

dataHandling(input)


// Nomor 5

console.log(" ")
console.log("Nomor 5")
console.log(" ")

function balikKata(kata){
    var hasil= "";
    for(index = kata.length; index > 0; index--) {
      hasil = hasil + kata[index-1];
    }  
    return hasil
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 


// Nomor 6

console.log(" ")
console.log("Nomor 6")
console.log(" ")

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input){
    input.splice(1, 1, input[1] + "Elsharawy");
    input.splice(2, 1, "Provinsi " + input[2]);
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input);

    var tanggal = input[3].split("/")
    var bulan = tanggal[1]
    switch(bulan) {
        case '01':   { console.log('Januari'); break; }
        case '02':   { console.log('Pebruari'); break; }
        case '03':   { console.log('Maret'); break; }
        case '04':   { console.log('April'); break; }
        case '05':   { console.log('Mei'); break; }
        case '06':   { console.log('Juni'); break; }
        case '07':   { console.log('Juli'); break; }
        case '08':   { console.log('Agustus'); break; }
        case '09':   { console.log('September'); break; }
        case '10':   { console.log('Oktober'); break; }
        case '11':   { console.log('November'); break; }
        case '12':   { console.log('Desember'); break; }
        default:  { break; }
    }
  
    tanggal.sort(function(a, b){return b-a}); 
    console.log(tanggal);
    console.log(input[3].split("/").join('-'));
    console.log(input[1].slice(0,15));
}

dataHandling2(input2);

