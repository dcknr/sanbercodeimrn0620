// Nomor 1

console.log(" ")
console.log("Nomor 1")
console.log(" ")

// Release 0
console.log("Release 0")
console.log(" ")

class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this._name   = name
        this._legs   = legs
        this._cold_blooded   = cold_blooded
    }
    get name() {
        return this._name   
    }
    get legs() {
        return this._legs
    }
    get cold_blooded() {
        return this._cold_blooded
    }
    set legs(leg) {
        this._legs = leg
    }
}
 
var sheep = new Animal("shaun")
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
console.log(" ")
console.log("Release 1")
console.log(" ")

class Ape extends Animal {
    constructor(name,legs=4,cold_blooded=false) {
      super(name,cold_blooded)
      this._legs = 2
    }
    yell() {
      console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name,legs=4,cold_blooded=false) {
      super(name,legs,cold_blooded)
    }
    jump() {
      console.log("hop hop")
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


// Nomor 2

console.log(" ")
console.log("Nomor 2")
console.log(" ")

class Clock {      

    constructor ({ template }) {
        this.template = template
    }    
    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

    console.log(output);
    }

    stop() {
        clearInterval(timer)
    };

    start() {
        this.render()
        this.timer = setInterval(this.render.bind(this), 1000)
        // this.timer = setInterval(() => this.render(), 1000)
    };
}

var clock = new Clock({template: 'h:m:s'})
clock.start()  
